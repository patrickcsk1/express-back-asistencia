const { verify } = require("jsonwebtoken");

module.exports = {
  checkToken: (req, res, next) => {
    let token = req.get("authorization");
    if (token) {
      token = token.slice(7);
      verify(token, process.env.KEY_TOKEN, (err, decoded) => {
      // verify(token, "Dropout404", (err, decoded) => {
        if (err) {
          return res.json({
            success: 0,
            message: "Token invalido..."
          });
        } else {
          req.decoded = decoded;
          next();
        }
      });
    } else {
      return res.json({
        success: 0,
        message: "Acceso Denegado! Usuario no Autorizado"
      });
    }
  }
};