const { getAsistencias, createAsistencia, takeAsistencia } = require("./asistencia.service");

module.exports = {
  getAsistencias: (req, res) => {
    const body = req.body;
    getAsistencias(body, (err, result) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "Error al cargar las asistencias"
        });
      }
      return res.status(200).json({
        success: 1,
        data: result
      });
    });
  },
  takeAsistencia: (req, res) => {
    const body = req.body;
    takeAsistencia(body, (err, result) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "Error al tomar asistencia"
        });
      }
      return res.status(200).json({
        success: 1,
        data: result
      });
    });
  },
  createAsistencia: (req, res) => {
    const body = req.body;
    createAsistencia(body, (err, result) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "Error al crear asistencia"
        });
      }
      return res.status(200).json({
        success: 1,
        data: result
      });
    });
  }
};
