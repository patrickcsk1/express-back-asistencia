const pool = require("../../config/database");

module.exports = {
  getAsistencias: (data, callback) => {
    var query = `select U.idUsuarios, U.liderId, a.*, F.* from Asistencias as a
    inner join Fechas F on a.idFecha = F.idFechas and F.idDia=?
    inner join Dias on F.idDia = Dias.idDias
    inner join Usuarios_has_Roles UhR on a.idMiembro = UhR.idUsuarioRol
    inner join Usuarios U on UhR.usuario_id_u = U.idUsuarios and U.eliminado=0`;
    pool.query(query, [data.idDia], (error, results, fields) => {
      if (error) return callback(error);
      return callback(null, results);
    });
  },

  createAsistencia: (data, callback) => {
    var query = `insert into Asistencias (asis, idFecha, idMiembro) values (?,?,?)`;
    pool.query(
      query,
      [data.asis, data.idFecha, data.idMiembro],
      (error, results, fields) => {
        if (error) return callback(error);
        return callback(null, results);
      }
    );
  },

  takeAsistencia: (data, callback) => {
    var query = `update Asistencias set asis = ? where idAsistencias= ?`;
    pool.query(
      query,
      [data.asis, data.idAsistencia],
      (error, results, fields) => {
        if (error) return callback(error);
        return callback(null, results);
      }
    );
  }
};
