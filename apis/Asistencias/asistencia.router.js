const { getAsistencias, createAsistencia, takeAsistencia } = require("./asistencia.controller");
const router = require("express").Router();

const { checkToken } = require("./../../auth/token_validation");

router.post("/all", checkToken, getAsistencias);
router.post("/create", checkToken, createAsistencia);
router.patch("/takeList", checkToken, takeAsistencia);

module.exports = router;