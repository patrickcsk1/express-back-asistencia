const {
  createUser,
  createUserRol,
  getUsers,
  getUserById,
  getUserRolId,
  login,
  updatePass,
  updateUser,
  updateLogin,
  deleteUser
} = require("./usuario.controller");
const router = require("express").Router();

const { checkToken } = require("./../../auth/token_validation");

router.post("/login", login);
router.post("/create", checkToken, createUser);
router.post("/crear", createUser);
router.post("/createUserRol", checkToken, createUserRol);
router.patch("/delete", checkToken, deleteUser);
router.post("/all", checkToken, getUsers);
router.post("/usuario", checkToken, getUserById);
router.post("/usuarioRol", checkToken, getUserRolId);
// router.post("/usuario1", getUserById);
router.patch("/updateLogin", checkToken, updateLogin);
router.patch("/contrasena", checkToken, updatePass);
router.patch("/update", checkToken, updateUser);

module.exports = router;
