const {
  createUser,
  createUserRol,
  getUsers,
  getUserById,
  getUserByUserEmail,
  getUserRolId,
  updatePass,
  updateLogin,
  updateUser,
  deleteUser
} = require("./usuario.service");
const bcrypt = require("bcrypt");
const { sign } = require("jsonwebtoken");

module.exports = {
  createUser: (req, res) => {
    const body = req.body;
    body.contrasena = bcrypt.hashSync(body.contrasena, 10);
    createUser(body, (err, result) => {
      if (err) {
        if (err.code === "ER_DUP_ENTRY") {
          console.log(err);
          return res.status(500).json({
            success: 0,
            message: "Usuario duplicado."
          });
        } else {
          console.log(err);
          return res.status(500).json({
            success: 0,
            message: "Error en la creación del usuario"
          });
        }
      }
      return res.status(200).json({
        success: 1,
        data: result
      });
    });
  },

  createUserRol: (req, res) => {
    const body = req.body;
    createUserRol(body, (err, result) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "Error al asignar rol al usuario"
        });
      }
      return res.status(200).json({
        success: 1,
        data: result
      });
    });
  },

  deleteUser: (req, res) => {
    const body = req.body;
    deleteUser(body, (err, result) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "Error borrar usuario"
        });
      }
      return res.status(200).json({
        success: 1,
        data: result
      });
    });
  },

  getUsers: (req, res) => {
    const body = req.body;
    getUsers(body, (err, result) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "Error de conección de la BD"
        });
      }
      return res.status(200).json({
        success: 1,
        data: result
      });
    });
  },

  getUserById: (req, res) => {
    const body = req.body;
    getUserById(body, (err, result) => {
      if (err || result===undefined) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "Usuario no hallado"
        });
      }
      return res.status(200).json({
        success: 1,
        data: result
      });
    });
  },

  getUserRolId: (req, res) => {
    const body = req.body;
    getUserRolId(body, (err, result) => {
      if (err || result===undefined) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "Usuario-Rol no hallado"
        });
      }
      return res.status(200).json({
        success: 1,
        data: result[0]
      });
    });
  },

  login: (req, res) => {
    const body = req.body;
    getUserByUserEmail(body, async (err, results) => {
      if (err) {
        console.log(err);
      }
      if (!results) {
        return res.json({
          success: 0,
          message: "Correo y/o contraseña invalida1"
        });
      }
      const result = bcrypt.compareSync(body.contrasena, results.contrasena);
      if (result) {
        results.contrasena = undefined;
        const key_token = process.env.KEY_TOKEN;
        const jsontoken = sign({ result: results }, key_token, {
          expiresIn: "1d"
        });
        return res.json({
          success: 1,
          message: "logueo exitoso",
          token: jsontoken,
          idUsuario: results.idUsuarios
        });
      } else {
        return res.json({
          success: 0,
          message: "Correo y/o contraseña invalida"
        });
      }
    });
  },

  updateLogin: (req, res) => {
    const body = req.body;
    updateLogin(body, (err, result) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "Error al actualizar login usuario"
        });
      }
      return res.status(200).json({
        success: 1,
        data: result
      });
    });
  },

  updatePass: (req, res) => {
    const body = req.body;
    body.contrasena = bcrypt.hashSync(body.contrasena, 10);
    updatePass(body, (err, result) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "Error al cambiar la contraseña"
        });
      }
      return res.status(200).json({
        success: 1,
        data: result
      });
    });
  },

  updateUser: (req, res) => {
    const body = req.body;
    updateUser(body, (err, result) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "Error al actualizar usuario"
        });
      }
      return res.status(200).json({
        success: 1,
        data: result
      });
    });
  },
};
