const pool = require("../../config/database");

module.exports = {
  createUser: (data, callback) => {
    pool.query(
      `insert into Usuarios(nombre, apellido, direccion, edad, fechaIngreso, telefono, usuario, contrasena, eliminado, unavez, liderId, login)
            value(?,?,?,?,?,?,?,?,?,?,?,?)`,
      [
        data.nombre,
        data.apellido,
        data.direccion,
        data.edad,
        data.fechaIngreso,
        data.telefono,
        data.usuario,
        data.contrasena,
        data.eliminado,
        data.unavez,
        data.liderId,
        data.login
      ],
      (error, results, fields) => {
        if (error) return callback(error);
        return callback(null, results);
      }
    );
  },

  createUserRol: (data, callback) => {
    pool.query(
      `insert into Usuarios_has_Roles (usuario_id_u, rol_id_r) values (?,?)`,
      [data.idUsuario, data.idRol],
      (error, results, fields) => {
        if (error) return callback(error);
        return callback(null, results);
      }
    );
  },

  deleteUser: (data, callback) => {
    var query = `update Usuarios set eliminado = 1 where idUsuarios=?`;
    pool.query(query, [data.idUsuario], (error, results, fields) => {
      if (error) return callback(error);
      return callback(null, results);
    });
  },

  getUserById: (data, callback) => {
    var query = `select u.*,r.* from (Usuarios as u, Roles as r)
    inner join Usuarios_has_Roles UhR on u.idUsuarios = UhR.usuario_id_u and r.idRoles = UhR.rol_id_r
    and u.eliminado=0 and u.idUsuarios=?`;
    pool.query(query, [data.idUsuario], (error, results, fields) => {
      if (error) return callback(error);
      return callback(null, results[0]);
    });
  },

  getUserByUserEmail: (data, callback) => {
    var query = `select * from Usuarios where usuario=? and eliminado=0`;
    pool.query(query, [data.usuario], (error, results, fields) => {
      if (error) {
        callback(error);
      }
      return callback(null, results[0]);
    });
  },

  getUserRolId: (data, callback) => {
    var query = `select idUsuarioRol from Usuarios_has_Roles where usuario_id_u=? and rol_id_r=?`;
    pool.query(query, [data.idUsuario, data.idRol], (error, results, fields) => {
      if (error) {
        callback(error);
      }
      return callback(null, results);
    });
  },

  //el data.id puede ser del admin o de algun lider.
  //el data.id corresponde a la tabla de UsuarioRol
  //si la opcion es usuario, el admin solicita admins/liders
  //si es otra opcion, el lider quiere ver a sus miembros
  getUsers: (data, callback) => {
    var query = ``;
    if (data.opcion == "admin") {
      query = `select u.*,r.* from (Usuarios as u, Roles as r)
    inner join Usuarios_has_Roles UhR on u.idUsuarios = UhR.usuario_id_u and r.idRoles = UhR.rol_id_r 
    and u.eliminado=0 and not(u.idUsuarios=?) and not(r.descripcion='Miembro')`;
    } else {
      query = `select u.*, r.* from Usuarios as u
      inner join Usuarios_has_Roles UhR on u.idUsuarios = UhR.usuario_id_u
      inner join Roles as r on UhR.rol_id_r = r.idRoles and u.eliminado=0 and u.liderId= ?`;
    }
    pool.query(query, [data.id], (error, results, fields) => {
      if (error) {
        callback(error);
      }
      return callback(null, results);
    });
  },

  updateLogin: (data, callback) => {
    var query = `update Usuarios set login = ? where idUsuarios= ?`;
    pool.query(
      query,
      [data.login, data.idUsuario],
      (error, results, fields) => {
        if (error) return callback(error);
        return callback(null, results);
      }
    );
  },

  updatePass: (data, callback) => {
    var query = `update Usuarios set contrasena = ?, unavez = 2 where idUsuarios=?`;
    pool.query(
      query,
      [data.contrasena, data.idUsuario],
      (error, results, fields) => {
        if (error) return callback(error);
        return callback(null, results);
      }
    );
  },

  updateUser: (data, callback) => {
    pool.query(
      `update Usuarios set nombre=?, apellido=?, edad=?, direccion=?, telefono=? where idUsuarios = ?`,
      [
        data.nombre,
        data.apellido,
        data.edad,
        data.direccion,
        data.telefono,
        data.idUsuario
      ],
      (error, results, fields) => {
        if (error) {
          callback(error);
        }
        return callback(null, results[0]);
      }
    );
  }
};
