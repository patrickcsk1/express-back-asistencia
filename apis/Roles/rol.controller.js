const { getRoles } = require("./rol.service");

module.exports = {
  getRoles: (req, res) => {
    getRoles((err, result) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "Error al cargar los roles"
        });
      }
      return res.status(200).json({
        success: 1,
        data: result
      });
    });
  },
};
