const pool = require("../../config/database");

module.exports = {
  getRoles: (callback) => {
    var query = `select * from Roles`;
    pool.query(query, [], (error, results, fields) => {
      if (error) return callback(error);
      return callback(null, results);
    });
  },
};
