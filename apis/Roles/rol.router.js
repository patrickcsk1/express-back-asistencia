const { getRoles } = require("./rol.controller");
const router = require("express").Router();

const { checkToken } = require("../../auth/token_validation");

router.get("/all", checkToken, getRoles);
// router.get("/all1", getRoles);

module.exports = router;