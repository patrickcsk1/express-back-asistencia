const { getDias, createDia, diaValid } = require("./dia.service");

module.exports = {
  getDias: (req, res) => {
    const body = req.body;
    getDias(body, (err, result) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "Error al cargar los dias"
        });
      }
      return res.status(200).json({
        success: 1,
        data: result
      });
    });
  },
  createDia: (req, res) => {
    const body = req.body;
    createDia(body, (err, result) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "Error al crear el dia"
        });
      }
      return res.status(200).json({
        success: 1,
        data: result
      });
    });
  },
  diaValid: (req, res) => {
    const body = req.body;
    diaValid(body, (err, result) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "Dia repetido"
        });
      }
      return res.status(200).json({
        success: 1,
        data: result
      });
    });
  }
};
