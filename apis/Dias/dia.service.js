const pool = require("../../config/database");

module.exports = {
  getDias: (data, callback) => {
    var query = `select * from Dias where idLider=?`;
    pool.query(query, [data.idLider], (error, results, fields) => {
      if (error) return callback(error);
      return callback(null, results);
    });
  },

  createDia: (data, callback) => {
    var query = `insert into Dias (descripcion, idLider) values (?,?);`;
    pool.query(query, [data.dia, data.idLider], (error, results, fields) => {
      if (error) return callback(error);
      return callback(null, results);
    });
  },

  diaValid: (data, callback) => {
    var query = `select * from Dias where idLider=? and descripcion=?`;
    pool.query(query, [data.idLider, data.dia], (error, results, fields) => {
      if (error) return callback(error);
      return callback(null, results);
    });
  }
};
