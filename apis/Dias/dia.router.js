const { getDias, createDia, diaValid } = require("./dia.controller");
const router = require("express").Router();

const { checkToken } = require("./../../auth/token_validation");

router.post("/all", checkToken, getDias);
router.post("/create", checkToken, createDia);
router.post("/diaValid", checkToken, diaValid);

module.exports = router;