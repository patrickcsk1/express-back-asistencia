const { getFechas, fechaValid, createFecha } = require("./fecha.service");

module.exports = {
  createFecha: (req, res) => {
    const body = req.body;
    createFecha(body, (err, result) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "Error al crear la fecha"
        });
      }
      return res.status(200).json({
        success: 1,
        data: result
      });
    });
  },
  getFechas: (req, res) => {
    const body = req.body;
    getFechas(body, (err, result) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "Error cargar las fechas"
        });
      }
      return res.status(200).json({
        success: 1,
        data: result
      });
    });
  },
  fechaValid: (req, res) => {
    const body = req.body;
    fechaValid(body, (err, result) => {
      if (err) {
        return res.status(500).json({
          success: 0,
          message: "Fecha repetida"
        });
      }
      return res.status(200).json({
        success: 1,
        data: result
      });
    });
  }
};
