const { getFechas, fechaValid, createFecha } = require("./fecha.controller");
const router = require("express").Router();

const { checkToken } = require("./../../auth/token_validation");

router.post("/all", checkToken, getFechas);
router.post("/fechaValid", checkToken, fechaValid);
router.post("/create", checkToken, createFecha);

module.exports = router;