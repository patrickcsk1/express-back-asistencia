const pool = require("../../config/database");

module.exports = {
  createFecha: (data, callback) => {
    var query = `insert into Fechas (fecha, idDia) values (?,?);`;
    pool.query(query, [data.fecha, data.idDia], (error, results, fields) => {
      if (error) return callback(error);
      return callback(null, results);
    });
  },
  getFechas: (data, callback) => {
    var query = `select f.* from Fechas as f
    inner join Dias as d on f.idDia = d.idDias
    inner join Usuarios_has_Roles as ur on ur.idUsuarioRol = d.idLider and ur.usuario_id_u = ? and ur.rol_id_r = ?`;
    pool.query(query, [data.idUsuario, data.idRol], (error, results, fields) => {
      if (error) return callback(error);
      return callback(null, results);
    });
  },
  fechaValid: (data, callback) => {
    var query = `select * from Fechas where idDia=? and fecha=?`;
    pool.query(query, [data.idDia, data.fecha], (error, results, fields) => {
      if (error) return callback(error);
      return callback(null, results);
    });
  }
};
