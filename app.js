require('dotenv').config();
const express = require('express');
const app = express();
const cors = require('cors');
const usuariosRouter = require("./apis/Usuarios/usuario.router");
const diasRouter = require("./apis/Dias/dia.router");
const rolRouter = require("./apis/Roles/rol.router");
const asistenciaRouter = require("./apis/Asistencias/asistencia.router");
const fechaRouter = require("./apis/Fechas/fecha.router");

app.use(express.json());
app.use(cors());

app.use("/api/usuarios", usuariosRouter);
app.use("/api/dias", diasRouter);
app.use("/api/roles", rolRouter);
app.use("/api/asistencias", asistenciaRouter);
app.use("/api/fechas", fechaRouter);

app.listen(process.env.APP_PORT, () => {
    console.log('Server started on port: ', process.env.APP_PORT);
})