const mysql = require("mysql");
// dotenv.config();

const hosti = process.env.DB_HOST;
const useri = process.env.DB_USUARIO;
const pass = process.env.DB_PASSWORD;
const datab = process.env.DB_SCHEMA;
const porti = process.env.DB_PORT;

const pool = mysql.createPool({
  host: hosti,
  user: useri,
  password: pass,
  database: datab,
  port: porti,
  // host: "db-asistencia-web.cwff81b7lotz.us-east-1.rds.amazonaws.com",
  // user: "admin",
  // password: "Innersystem404",
  // database: "asistencia",
  // port: "3306",
  connectionLimit: 10
});

pool.getConnection((err, connection) => {
  if (err) {
    if (err.code === "PROTOCOL_CONNECTION_LOST") {
      console.error("Database connection was closed.");
    }
    if (err.code === "ER_CON_COUNT_ERROR") {
      console.error("Database has to many connections");
    }
    if (err.code === "ECONNREFUSED") {
      console.error("Database connection was refused");
    }
  }

  if (connection) {
    connection.release();
    console.log("DB is Connected");
  }

  return;
});

module.exports = pool;
